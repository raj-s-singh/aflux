package de.tum.in.aflux.flux_engine;


/**
 * Interface defined for methods of JobDeployer implementation
 *
 *
 */
import java.util.List;

public  interface JobDeployer {


    /**
    *Method to deploy the Job file to a flink cluster
     */
    public void deployFlinkJob(String flinkClusterIP, String flinkClusterPort, String jarFile, String parallelism) throws Exception;

    public void initializeKafkaConsumer(String topicName);
    public void subscribeKafkaTopics(List<String> topicNames);
    public void consumeKafkaTopics(long pollingTime);
    public void disconnectConsumer();
    public List<String> concatMessages(List<String> fluxOutput);

    }