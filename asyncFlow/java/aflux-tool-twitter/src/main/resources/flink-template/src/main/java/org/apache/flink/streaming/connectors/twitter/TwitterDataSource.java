package org.apache.flink.streaming.connectors.twitter;

import java.util.List;

/**
 * Data source that can be used in Flink jobs and retrieves data from the twitter
 * stream
 */
public class TwitterDataSource extends TwitterSource {

	/**
	 * List of Terms to be tracked by the source
	 */
	private final List<String> trackTerms;

	public TwitterDataSource(List<String> trackTerms) {
		super(TwitterCredentials.getTwitterCredentials());
		super.setCustomEndpointInitializer(new TweetFilter(trackTerms));
		this.trackTerms = trackTerms;
	}

}
