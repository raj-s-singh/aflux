package org.apache.flink.streaming.connectors.twitter;

import java.io.Serializable;
import java.util.List;

import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.endpoint.StreamingEndpoint;

public class TweetFilter implements TwitterSource.EndpointInitializer, Serializable {
    private List<String> trackTerms;

    public TweetFilter(List<String> trackTerms){
        this.trackTerms = trackTerms;
    }

    @Override
    public StreamingEndpoint createEndpoint() { 
        StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint(); 
        endpoint.trackTerms(trackTerms); 
        return endpoint; 
    } 
}
