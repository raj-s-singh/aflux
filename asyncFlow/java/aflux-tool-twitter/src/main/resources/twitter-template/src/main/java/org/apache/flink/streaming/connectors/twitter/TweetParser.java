package org.apache.flink.streaming.connectors.twitter;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;

public class TweetParser implements FlatMapFunction<String, Tweet> {

    /**
     * The core method of the FlatMapFunction. Takes an element from the input data set and transforms
     * it into zero, one, or more elements.
     *
     * @param value The input value.
     * @param collector   The collector for returning result values.
     * @throws Exception This method may throw exceptions. Throwing an exception will cause the operation
     *                   to fail and may trigger recovery.
     */
    @Override
    public void flatMap(String value, Collector<Tweet> collector) throws Exception {
        Tweet tweet = Tweet.fromString(value);
        if (tweet != null) {
            collector.collect(tweet);
        }
    }
}
