package org.apache.flink.streaming.connectors.twitter;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Tweet {

    public String text;
    public String userName;
    private String rawText;
    public String lang;
    public String source;
    public int userFollowerCount;

    private Tweet() {
    }

    public static Tweet fromString(String s) {

        ObjectMapper jsonParser = new ObjectMapper();
        Tweet tweet = new Tweet();
        tweet.rawText = s;

        try {
            JsonNode node = jsonParser.readValue(s, JsonNode.class);
            Boolean isLang = node.has("user") && node.get("user").has("lang");// &&
            // !node.get("user").get("lang").asText().equals("null");

            if(isLang && node.has("text"))
            {
                JsonNode userNode = node.get("user");
                tweet.text = node.get("text").asText();
                tweet.userName = userNode.get("name").asText();
                tweet.lang = node.get("lang").asText();
                tweet.userFollowerCount = userNode.get("followers_count").asInt();

                if(node.has("source")){

                    String source = node.get("source").asText().toLowerCase();
                    if(source.contains("android"))
                        source = "Android";
                    else if (source.contains("iphone"))
                        source="iphone";
                    else if (source.contains("web"))
                        source="web";
                    else
                        source="unknown";

                    tweet.source =source;
                }


                return tweet;
            }

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public String toString() {
        return this.userName+ "; "
                +this.lang + "; "
                +this.source + "; "
                +this.text + "; "
                +this.userFollowerCount ;
        //+this.rawText + " ";
    }
}