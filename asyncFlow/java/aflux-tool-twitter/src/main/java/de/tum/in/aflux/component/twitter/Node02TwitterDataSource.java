package de.tum.in.aflux.component.twitter;

import de.tum.in.aflux.component.twitter.actor.TwitterDataSourceActor;
import de.tum.in.aflux.component.twitter.api.FlinkApiMapper;
import de.tum.in.aflux.tools.core.*;

import java.util.stream.Stream;

public class Node02TwitterDataSource extends AbstractMainExecutor {

    public static final FlinkApiMapper API = FlinkApiMapper.getInstance();

    public static final String NAME = "Twitter Data";
    public static final String PROPERTY_TRACKING_TERMS = "Tags to be tracked";
    public static final String PROPERTY_OBSERVATION_TYPE = "Tags to be tracked";


    public static final ToolSemanticsCondition[] semanticsConditions = {
            new ToolSemanticsCondition(
                    Node01EnvironmentSetUp.class,
                    true,
                    true,
                    true)
    };

    public static ToolProperty trackTermProperty = new ToolProperty(
            PROPERTY_TRACKING_TERMS,
            "Microsoft",
            PropertyInputType.TEXT,
            null,
            "List of tags to be tracked by Twitter Source",
            "",
            false);

    public Node02TwitterDataSource() {
        super(Node02TwitterDataSource.NAME,
                TwitterDataSourceActor.class.getCanonicalName(),
                Node02TwitterDataSource.class.getName(),
                1,
                1,
                NodeLaunchType.LAUNCHED_BY_DATA,
                false,
                null,
                semanticsConditions);

        ToolProperty[] properties = this.getProperties();
        properties = Stream.of(properties, new ToolProperty[]{trackTermProperty})
                .flatMap(Stream::of)
                .toArray(ToolProperty[]::new);
        this.setProperties(properties);
        this.setColor("#288BA8");
    }

}
