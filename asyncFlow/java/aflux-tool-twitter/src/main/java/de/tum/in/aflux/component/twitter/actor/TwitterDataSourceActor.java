package de.tum.in.aflux.component.twitter.actor;

import com.squareup.javapoet.*;
import de.tum.in.aflux.component.twitter.Node02TwitterDataSource;
import de.tum.in.aflux.component.twitter.api.FlinkApiMapper;
import de.tum.in.aflux.component.twitter.util.FlinkFlowMessage;
import de.tum.in.aflux.component.twitter.util.JavaCodeGenerator;
import de.tum.in.aflux.flux_engine.FluxEnvironment;
import de.tum.in.aflux.flux_engine.FluxRunner;
import de.tum.in.aflux.tools.core.AbstractAFluxActor;


import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This actor is in charge of generating the code to read data from the Twitter API.
 * Example output:
 *
 * <pre>
 *     TwitterSource<...> source = new TwitterSource<>(...);
 *     DataStream<...> output = see.addSource(source, TypeInformation.of(...))
 *                                 .assignTimestampsAndWatermarks(...);
 * </pre>
 */
public class TwitterDataSourceActor extends AbstractAFluxActor {

    public static final FlinkApiMapper API = FlinkApiMapper.getInstance();

    public TwitterDataSourceActor(String fluxId, FluxEnvironment fluxEnvironment, FluxRunner fluxRunner, Map<String,String> properties) {
        super(fluxId, fluxEnvironment, fluxRunner, properties, -1);
    }

    @Override
    protected void runCore(Object message) throws Exception {

        // Validate and cast message
        FlinkFlowMessage msg;
        try {
            msg = FlinkFlowMessage.fromRawMessage(message);
        } catch(IllegalArgumentException e) {
            this.sendOutput("Error when receiving message from previous node.");
            return;
        }
        CodeBlock.Builder code = msg.getCode();

        // Get properties of node
        String trackingTermsProperty = this.getProperty(Node02TwitterDataSource.PROPERTY_TRACKING_TERMS);

        // Define types that will be used
        TypeName parameterizedDataStreamType = ParameterizedTypeName.get(
                API.getClassNameInstance("DataStream"),
                API.getClassNameInstance("Tweet")); // e.g. DataStream<Tweet>
        

        // Add code
        this.sendOutput("Generating code for: Twitter data source and stream");

        Map<String, Object> codeVariables = new LinkedHashMap<>();
        codeVariables.put("dataStreamType", parameterizedDataStreamType);
        codeVariables.put("variable1", JavaCodeGenerator.newVariableName());
        codeVariables.put("env", EnvironmentSetUpActor.GENERATED_CODE_VARIABLE_ENV);
        codeVariables.put("addSource", API.getMethodName("StreamExecutionEnvironment.addSource"));
        codeVariables.put("twitterDataSource", API.getClassNameInstance("TwitterDataSource"));
        codeVariables.put("arraysType", TypeName.get(Arrays.class));
        codeVariables.put("trackingTerms", trackingTermsProperty);
        codeVariables.put("tweetParser", API.getClassNameInstance("TweetParser"));

        code.addNamed( // define data stream tweet
                "$dataStreamType:T $variable1:L = $env:L.$addSource:L(new " +
                        "$twitterDataSource:T($arraysType:T.asList(\"$trackingTerms:L\")))" +
                        ".flatMap(new $tweetParser:T());\n"
                , codeVariables);

        // Output code builder for next actor
        msg.setCurrentType(parameterizedDataStreamType);
        msg.setCurrentDataStreamVariableName((String)(codeVariables.get("variable1")));
        this.setOutput(1, msg);
    }
}
