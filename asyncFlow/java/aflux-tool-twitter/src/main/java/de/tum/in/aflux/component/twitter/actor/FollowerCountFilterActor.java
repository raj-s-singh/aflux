package de.tum.in.aflux.component.twitter.actor;

import java.util.LinkedHashMap;
import java.util.Map;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;

import de.tum.in.aflux.component.twitter.Node05FollowerCountFilter;
import de.tum.in.aflux.component.twitter.api.FlinkApiMapper;
import de.tum.in.aflux.component.twitter.util.FlinkFlowMessage;
import de.tum.in.aflux.component.twitter.util.JavaCodeGenerator;
import de.tum.in.aflux.flux_engine.FluxEnvironment;
import de.tum.in.aflux.flux_engine.FluxRunner;
import de.tum.in.aflux.tools.core.AbstractAFluxActor;

public class FollowerCountFilterActor extends AbstractAFluxActor{
    public static final FlinkApiMapper API = FlinkApiMapper.getInstance();

    public FollowerCountFilterActor(String fluxId, FluxEnvironment fluxEnvironment, FluxRunner fluxRunner, Map<String,String> properties) {
        super(fluxId, fluxEnvironment, fluxRunner, properties, -1);
    }

    @Override
    protected void runCore(Object message) throws Exception {
        // Validate and cast message
        FlinkFlowMessage msg;
        try {
            msg = FlinkFlowMessage.fromRawMessage(message);
        } catch(IllegalArgumentException e) {
            this.sendOutput("Error when receiving message from previous node.");
            return;
        }
        CodeBlock.Builder code = msg.getCode();
        TypeName inputType = msg.getCurrentType();
        String inputVariableName = msg.getCurrentDataStreamVariableName();

        // Get properties of node
        int followerCount = Integer.parseInt(this.getProperty(Node05FollowerCountFilter.PROPERTY_FOLLOWER_COUNT));

        // Define types that will be used
        TypeName mapInputType = ((ParameterizedTypeName)inputType).typeArguments.get(0); // Tweet
        TypeName parameterizedDataStreamType = ParameterizedTypeName.get(
            API.getClassNameInstance("DataStream"),
            mapInputType); // e.g. DataStream<Tweet>
        TypeName parameterizedFilterFunctionType = ParameterizedTypeName.get(
            API.getClassNameInstance("FilterFunction"),
            mapInputType); // e.g. FilterFunction<Tweet>
    

        // Add code
        this.sendOutput("Generating code for: Follower Count Filter Actor");

        Map<String, Object> codeVariables = new LinkedHashMap<>();
        codeVariables.put("dataStreamType", parameterizedDataStreamType);
        codeVariables.put("variable1", JavaCodeGenerator.newVariableName());
        codeVariables.put("inputVar", inputVariableName);
        codeVariables.put("filterFunction", parameterizedFilterFunctionType);
        codeVariables.put("tweetType",mapInputType);
        codeVariables.put("followers",followerCount);

        code.addNamed( // define data stream tweet
                "$dataStreamType:T $variable1:L = $inputVar:L.filter(new " +
                        "$filterFunction:T(){\n"+
                        "\t@Override\n"+
                        "\tpublic boolean filter($tweetType:T tweet) throws Exception{\n"+
                        "\t\t return tweet.userFollowerCount > $followers:L;\n" +
                        "\t}\n"+
                        "});\n"
                , codeVariables);

        // Output code builder for next actor
        msg.setCurrentType(parameterizedDataStreamType);
        msg.setCurrentDataStreamVariableName((String)(codeVariables.get("variable1")));
        this.setOutput(1, msg);

    }
    
}
