package de.tum.in.aflux.component.twitter;

import de.tum.in.aflux.component.twitter.actor.LanguageFilterActor;
import de.tum.in.aflux.tools.core.*;


public class Node04LanguageFilter extends AbstractMainExecutor{
    public static final String NAME = "Tweet Language Filter";
    public static final String PROPERTY_LANGUAGE_TYPE = "Tweet Language";

    public static final ToolSemanticsCondition[] semanticsConditions = {
        new ToolSemanticsCondition(
            Node01EnvironmentSetUp.class,
            true,
            false,
            true),
        new ToolSemanticsCondition(
            Node02TwitterDataSource.class,
            true,
            false,
            true)
    };

    public static ToolProperty[] properties = {
        new ToolProperty(
                PROPERTY_LANGUAGE_TYPE,
                "en",
                PropertyInputType.TEXT,
                null,
                "Enter language tag(BCP47) of the tweet to filter in",
                "",
                false)
    };

    public Node04LanguageFilter() {
        super(Node04LanguageFilter.NAME,
                LanguageFilterActor.class.getCanonicalName(),
                Node04LanguageFilter.class.getName(),
                1,
                1,
                NodeLaunchType.LAUNCHED_BY_DATA,
                false,
                properties,
                semanticsConditions);
        this.setColor("#288BA8");
    }

}
