package de.tum.in.aflux.component.twitter.util;

import javax.lang.model.element.Modifier;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.JavaFile;

public class FlinkFlowMessage {

    private CodeBlock.Builder code;
    private TypeName currentType;
    private String currentDataStreamVariableName;
    private String currentPatternVariableName;

    // is changed to true once the ExecuteAndGenerateJob actor has completed execution.s
    private boolean flowMessageStatus = false; 

    public static final String GENERATED_CLASS_NAME = "TweetFilterJob";
    public static final String GENERATED_PACKAGE_NAME = "de.tum.in.flink";


    public FlinkFlowMessage(CodeBlock.Builder code) {
        this.code = code;
        this.currentType = null;
        this.currentDataStreamVariableName = null;
    }

    public String getCurrentDataStreamVariableName() {
        return currentDataStreamVariableName;
    }

    public void setCurrentDataStreamVariableName(String currentDataStreamVariableName) {
        this.currentDataStreamVariableName = currentDataStreamVariableName;
    }

    public String getCurrentPatternVariableName() {
        return currentPatternVariableName;
    }

    public void setCurrentPatternVariableName(String currentPatternVariableName) {
        this.currentPatternVariableName = currentPatternVariableName;
    }

    public CodeBlock.Builder getCode() {
        return code;
    }

    public void setCode(CodeBlock.Builder code) {
        this.code = code;
    }

    public TypeName getCurrentType() {
        return currentType;
    }

    public void setCurrentType(TypeName currentType) {
        this.currentType = currentType;
    }

    public static FlinkFlowMessage fromRawMessage(Object message) throws IllegalStateException {
        if ((message != null) && (message instanceof FlinkFlowMessage)) {
            return (FlinkFlowMessage) message;
        } else {
            throw new IllegalArgumentException();
        }
    }



    public void setFlowMessageStatus(){
        this.flowMessageStatus = true;
    }

    /*
    * Method to get the Flink Code in the form of a String for the Mapper.
    * This method should - 
    * 1. Return the TypeSpec till the CodeBlock doesnt include statements from all Actors.
    * 2. Return the JavaFile once the CodeBlock when the ExecuteAndGenerateJob Actor has added its code.
    */
    @Override
    public String toString(){
        CodeBlock.Builder codeStatements = this.code;
        
        MethodSpec.Builder main = MethodSpec.methodBuilder("main")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addException(Exception.class)
                .returns(void.class)
                .addParameter(String[].class, "args");
        
        main.addCode(codeStatements.build());
        
        TypeSpec generatedClass = TypeSpec.classBuilder(GENERATED_CLASS_NAME)
                .addModifiers(Modifier.PUBLIC)
                .addMethod(main.build())
                .build();
        JavaFile javaFile = JavaFile.builder(GENERATED_PACKAGE_NAME, generatedClass)
            .build();
        
        // if(this.flowMessageStatus){
        //     JavaFile javaFile = JavaFile.builder(GENERATED_PACKAGE_NAME, generatedClass)
        //         .build();

        //     return javaFile.toString();
        // }

        return javaFile.toString();
    }

}
