package de.tum.in.aflux.component.twitter;

import de.tum.in.aflux.component.twitter.actor.DeviceFilterActor;
import de.tum.in.aflux.tools.core.*;

public class Node03DeviceFilter extends AbstractMainExecutor {
    public static final String NAME = "Device Filter";
    public static final String PROPERTY_DEVICE_TYPE = "Device Type";

    public static final ToolPropertyOption[] DEVICE_TYPES = {
        new ToolPropertyOption("Unknown", "unknown"),
        new ToolPropertyOption("Android", "android"),
        new ToolPropertyOption("Web", "web"),
        new ToolPropertyOption("Iphone", "iphone")
    };

    public static final ToolSemanticsCondition[] semanticsConditions = {
        new ToolSemanticsCondition(
            Node01EnvironmentSetUp.class,
            true,
            false,
            true),
        new ToolSemanticsCondition(
            Node02TwitterDataSource.class,
            true,
            false,
            true)
    };

    public static ToolProperty[] properties = {
        new ToolProperty(
                PROPERTY_DEVICE_TYPE,
                "Unknown",
                PropertyInputType.SELECT,
                DEVICE_TYPES,
                "Choose the type of device to filter out",
                "",
                false)
    };

    public Node03DeviceFilter() {
        super(Node03DeviceFilter.NAME,
                DeviceFilterActor.class.getCanonicalName(),
                Node03DeviceFilter.class.getName(),
                1,
                1,
                NodeLaunchType.LAUNCHED_BY_DATA,
                false,
                properties,
                semanticsConditions);
        this.setColor("#288BA8");
    }
}
