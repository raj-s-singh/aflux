package de.tum.in.aflux.component.twitter.actor;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;

import de.tum.in.aflux.component.twitter.Node04LanguageFilter;
import de.tum.in.aflux.component.twitter.api.FlinkApiMapper;
import de.tum.in.aflux.component.twitter.util.FlinkFlowMessage;
import de.tum.in.aflux.component.twitter.util.JavaCodeGenerator;
import de.tum.in.aflux.flux_engine.FluxEnvironment;
import de.tum.in.aflux.flux_engine.FluxRunner;
import de.tum.in.aflux.tools.core.AbstractAFluxActor;

public class LanguageFilterActor extends AbstractAFluxActor{
    public static final FlinkApiMapper API = FlinkApiMapper.getInstance();

    public LanguageFilterActor(String fluxId, FluxEnvironment fluxEnvironment, FluxRunner fluxRunner, Map<String,String> properties) {
        super(fluxId, fluxEnvironment, fluxRunner, properties, -1);
    }

    @Override
    protected void runCore(Object message) throws Exception {
        // Validate and cast message
        FlinkFlowMessage msg;
        try {
            msg = FlinkFlowMessage.fromRawMessage(message);
        } catch(IllegalArgumentException e) {
            this.sendOutput("Error when receiving message from previous node.");
            return;
        }
        CodeBlock.Builder code = msg.getCode();
        TypeName inputType = msg.getCurrentType();
        String inputVariableName = msg.getCurrentDataStreamVariableName();

        // Get properties of node
        String languageType = this.getProperty(Node04LanguageFilter.PROPERTY_LANGUAGE_TYPE);

        // Define types that will be used
        TypeName mapInputType = ((ParameterizedTypeName)inputType).typeArguments.get(0); // Tweet
        TypeName parameterizedDataStreamType = ParameterizedTypeName.get(
            API.getClassNameInstance("DataStream"),
            mapInputType); // e.g. DataStream<Tweet>
        TypeName parameterizedFilterFunctionType = ParameterizedTypeName.get(
            API.getClassNameInstance("FilterFunction"),
            mapInputType); // e.g. FilterFunction<Tweet>
        TypeName parameterizedLocaleTypeName = ParameterizedTypeName.get(
            ClassName.get("java.util", "Set")
            ,ClassName.get("java.util", "Locale")); // e.g. Set<Locale>

        // Add code
        this.sendOutput("Generating code for: Language Filter Actor");

        Map<String, Object> codeVariables = new LinkedHashMap<>();
        codeVariables.put("dataStreamType", parameterizedDataStreamType);
        codeVariables.put("variable1", JavaCodeGenerator.newVariableName());
        codeVariables.put("variable2", JavaCodeGenerator.newVariableName());
        codeVariables.put("inputVar", inputVariableName);
        codeVariables.put("filterFunction", parameterizedFilterFunctionType);
        codeVariables.put("tweetType",mapInputType);
        codeVariables.put("language",languageType);
        codeVariables.put("localeType",ClassName.get("java.util", "Locale"));
        codeVariables.put("setType",parameterizedLocaleTypeName);
        codeVariables.put("arraysType",ClassName.get("java.util", "Arrays"));
        codeVariables.put("hashsetType",ClassName.get("java.util", "HashSet"));
        

        code.addNamed( // define data stream tweet
                "$dataStreamType:T $variable1:L = $inputVar:L.filter(new " +
                        "$filterFunction:T(){\n"+
                        "\t@Override\n"+
                        "\tpublic boolean filter($tweetType:T tweet) throws Exception{\n"+
                        "\t\t $localeType:T $variable2:L = new $localeType:T.Builder().setLanguage($language:S).build();\n"+
                        "\t\t $setType:T hSet = new $hashsetType:T($arraysType:T.asList($localeType:T.getAvailableLocales()));\n"+
                        "\t\t if(!hSet.contains($variable2:L)) throw new Exception(\"The API doesnot provide support for this language tag.\");\n"+
                        "\t\t return tweet.lang.equals($language:S);\n" +
                        "\t}\n"+
                        "});\n"
                , codeVariables);

        // Output code builder for next actor
        msg.setCurrentType(parameterizedDataStreamType);
        msg.setCurrentDataStreamVariableName((String)(codeVariables.get("variable1")));
        this.setOutput(1, msg);

    }
    
}