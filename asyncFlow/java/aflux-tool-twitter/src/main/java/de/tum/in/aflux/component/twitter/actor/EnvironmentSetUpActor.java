package de.tum.in.aflux.component.twitter.actor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;

import org.apache.flink.api.common.restartstrategy.RestartStrategies;

import de.tum.in.aflux.component.twitter.api.FlinkApiMapper;
import de.tum.in.aflux.component.twitter.util.FlinkFlowMessage;
import de.tum.in.aflux.flux_engine.FluxEnvironment;
import de.tum.in.aflux.flux_engine.FluxRunner;
import de.tum.in.aflux.tools.core.AbstractAFluxActor;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This actor is in charge of generating the code to set-up Flink and its environment.
 * Example output:
 *
 * <pre>
 *     StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
 * </pre>
 */
public class EnvironmentSetUpActor extends AbstractAFluxActor {

    public static final FlinkApiMapper API = FlinkApiMapper.getInstance();

    public static final String GENERATED_CODE_VARIABLE_ENV = "env";

    public EnvironmentSetUpActor(String fluxId, FluxEnvironment fluxEnvironment, FluxRunner fluxRunner, Map<String,String> properties) {
        super(fluxId, fluxEnvironment, fluxRunner, properties, -1);
    }

    @Override
    protected void runCore(Object message) throws Exception {
        CodeBlock.Builder code = CodeBlock.builder();

        this.sendOutput("Generating code for: environment set-up");
        Map<String, Object> codeVariables = new LinkedHashMap<>();
        codeVariables.put("execEnvType", API.getClassNameInstance("StreamExecutionEnvironment"));
        codeVariables.put("env", GENERATED_CODE_VARIABLE_ENV);
        codeVariables.put("restartStratType",RestartStrategies.class);

        code.addStatement("$T $L = $T.$L()",
            API.getClassNameInstance("StreamExecutionEnvironment"),
            GENERATED_CODE_VARIABLE_ENV,
            API.getClassNameInstance("StreamExecutionEnvironment"),
            API.getMethodName("StreamExecutionEnvironment.getExecutionEnvironment"));
        code.addStatement("$L.setRestartStrategy($T.noRestart())",
            GENERATED_CODE_VARIABLE_ENV,
            ClassName.get("org.apache.flink.api.common.restartstrategy", "RestartStrategies"));
        code.addStatement("$L.getCheckpointConfig().setCheckpointTimeout(10000)",
            GENERATED_CODE_VARIABLE_ENV);
        code.addStatement("$L.disableOperatorChaining()",
            GENERATED_CODE_VARIABLE_ENV);
        // output code builder for next actor
        this.setOutput(1, new FlinkFlowMessage(code));
    }

}
