package de.tum.in.aflux.component.twitter;

import de.tum.in.aflux.component.twitter.actor.FollowerCountFilterActor;
import de.tum.in.aflux.tools.core.*;


public class Node05FollowerCountFilter extends AbstractMainExecutor{
    public static final String NAME = "Follower Count Filter";
    public static final String PROPERTY_FOLLOWER_COUNT = "Follower Count";

    public static final ToolSemanticsCondition[] semanticsConditions = {
        new ToolSemanticsCondition(
            Node01EnvironmentSetUp.class,
            true,
            false,
            true),
        new ToolSemanticsCondition(
            Node02TwitterDataSource.class,
            true,
            false,
            true)
    };

    public static ToolProperty[] properties = {
        new ToolProperty(
                PROPERTY_FOLLOWER_COUNT,
                "0",
                PropertyInputType.TEXT,
                null,
                "Enter the min follower count to filter in",
                "",
                false)
    };

    public Node05FollowerCountFilter() {
        super(Node05FollowerCountFilter.NAME,
                FollowerCountFilterActor.class.getCanonicalName(),
                Node05FollowerCountFilter.class.getName(),
                1,
                1,
                NodeLaunchType.LAUNCHED_BY_DATA,
                false,
                properties,
                semanticsConditions);
        this.setColor("#288BA8");
    }

}
