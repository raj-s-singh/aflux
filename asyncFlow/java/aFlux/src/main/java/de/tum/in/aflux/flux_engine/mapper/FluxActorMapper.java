/*
 * aFlux: JVM based IoT Mashup Tool
 * Copyright 2019 Tanmaya Mahapatra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.tum.in.aflux.flux_engine.mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Mapper for Actor and their corresponding code statements
 */
public class FluxActorMapper {
    static List<MapperEntry> fluxMap;
    static int sizeOfList;

    public static void instantiateMapper(){
        FluxActorMapper.fluxMap = new ArrayList<>();
        FluxActorMapper.sizeOfList=0;
    }

    public static void addActortoMapper(String executorClassName, int actorBeginPosition, int actorEndPosition ){
        FluxActorMapper.sizeOfList+=1;
        FluxActorMapper.fluxMap.add(new MapperEntry(FluxActorMapper.sizeOfList,executorClassName,actorBeginPosition,actorEndPosition));
    }


    public static List<MapperEntry> getFluxMap() {
        return FluxActorMapper.fluxMap;
    }
    
    public static int getSizeOfList() {
        return FluxActorMapper.sizeOfList;
    }

    public static void incrementFluxMap(int noOfLines){
        for(MapperEntry element: FluxActorMapper.fluxMap){
            element.setActorBeginPosition(element.getActorBeginPosition()+noOfLines);
            element.setActorEndPosition(element.getActorEndPosition()+noOfLines);
        }
    }

    @Override
    public String toString() {
        var result=new StringBuilder();
        for(MapperEntry element: FluxActorMapper.fluxMap){
            result.append(element.toString()+"\n");
        }
        return result.toString();
    }

    
}
