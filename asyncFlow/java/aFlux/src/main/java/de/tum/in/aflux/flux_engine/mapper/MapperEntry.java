/*
 * aFlux: JVM based IoT Mashup Tool
 * Copyright 2019 Tanmaya Mahapatra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.tum.in.aflux.flux_engine.mapper;



/**
 * Mapper Entry for constructing 
 */
public class MapperEntry {
    private int actorNo;
    private String executorClassName;
    private int actorBeginPosition;
    private int actorEndPosition;

    public MapperEntry(int actorNo, String executorClassName, int actorBeginPosition, int actorEndPosition) {
        this.actorNo = actorNo;
        this.executorClassName = executorClassName;
        this.actorBeginPosition = actorBeginPosition;
        this.actorEndPosition = actorEndPosition;
    }


    public int getActorNo() {
        return this.actorNo;
    }

    public void setActorNo(int actorNo) {
        this.actorNo = actorNo;
    }

    public String getExecutorClassName() {
        return this.executorClassName;
    }

    public void setExecutorClassName(String executorClassName) {
        this.executorClassName = executorClassName;
    }

    public int getActorBeginPosition() {
        return this.actorBeginPosition;
    }

    public void setActorBeginPosition(int actorBeginPosition) {
        this.actorBeginPosition = actorBeginPosition;
    }

    public int getActorEndPosition() {
        return this.actorEndPosition;
    }

    public void setActorEndPosition(int actorEndPosition) {
        this.actorEndPosition = actorEndPosition;
    }

    @Override
    public String toString() {
        return "{" +
            " actorNo='" + getActorNo() + "'" +
            ", executorClassName='" + getExecutorClassName() + "'" +
            ", actorBeginPosition='" + getActorBeginPosition() + "'" +
            ", actorEndPosition='" + getActorEndPosition() + "'" +
            "}";
    }


}
