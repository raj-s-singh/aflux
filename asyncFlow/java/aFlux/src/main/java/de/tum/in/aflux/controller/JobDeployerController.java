package de.tum.in.aflux.controller;


import de.tum.in.aflux.flux_engine.impl.JobDeployerImpl;
import de.tum.in.aflux.model.FlowJob;
import de.tum.in.aflux.util.AFluxUtils;
import de.tum.in.aflux.util.FileSystemUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * class to manage requests related
 * to deployment of jobs to Flink Engine
 *
 */



@CrossOrigin(origins="http://localhost:3000")
@Controller

public class JobDeployerController {

    /**
     *
     */
    private static final String FLINK_TEMPLATE = "/home/ec2-user/template/target/aflux-packaged-1.0-SNAPSHOT.jar";
    @Autowired
    JobDeployerImpl jobDeployer;

    @GetMapping(value = "/getPackagedJarFile", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void getPackagedJarFile(HttpServletResponse response) throws IOException{
        byte[] reportBytes = null;
        String fileName = FLINK_TEMPLATE;
        File result=new File(fileName);

        if(result.exists()){
            try(InputStream inputStream = new FileInputStream(fileName)){
                String type=URLConnection.guessContentTypeFromName(fileName);
                response.setHeader("Content-Disposition", "attachment; filename=\"flink-sandbox-1.0-SNAPSHOT.jar");
                response.setHeader("Content-Type",type);
    
                reportBytes=new byte[100];//New change
                OutputStream os=response.getOutputStream();//New change
                int read=0;
                while((read=inputStream.read(reportBytes))!=-1){
                    os.write(reportBytes,0,read);
                }
                os.flush();
                os.close();
            }
        }
    }

    @PostMapping(value="/deployFlinkJob/{jobName}/{activityID}")
    public @ResponseBody FlowJob deployFlinkJob(@RequestParam("jarFile") MultipartFile jarFile,
     @RequestParam("flinkClusterIP") String flinkClusterIP, @RequestParam("flinkClusterPort") String flinkClusterPort
     , @RequestParam("parallelism") String parallelism, @PathVariable String jobName, @PathVariable Long activityID){
        var filePath="";
        if (!jarFile.isEmpty()) {
			String uploadBaseDir=FileSystemUtil.getUploadBaseDir();
			String jarLocation=AFluxUtils.getNowId()+"/";
			String fileDir=uploadBaseDir+jarLocation;
	        String originalName = jarFile.getOriginalFilename();
	        if(! new File(fileDir).exists())
	        {
	            new File(fileDir).mkdir();
	        }			
	        filePath = fileDir + originalName;
	        var dest = new File(filePath);
	        try {
                jarFile.transferTo(dest);
            } catch (IllegalStateException | IOException e) {
                e.printStackTrace();
            }
		}
        return jobDeployer.deployFlinkJob(flinkClusterIP, flinkClusterPort, filePath, parallelism, jobName, activityID);
    }

    
    @PostMapping(value="/consumeKafkaTopic")
    public @ResponseBody void consumeKafkaTopic(@RequestParam("kafkaConsumerIP") String kafkaConsumerIP, @RequestParam("kafkaConsumerPort") String kafkaConsumerPort
     , @RequestParam("consumerGroupId") String consumerGroupId, @RequestParam("sessionTimeout") String sessionTimeout, @RequestParam("pollTimeLimit") String pollTimeLimit,
     @RequestParam("topicName") String topicName){
        // System.out.println("DEBUG HERE ----> GRP ID"+ consumerGroupId + " SESSION TIMEOUT " + sessionTimeout);
        try{
            jobDeployer.consumeKafkaTopic(kafkaConsumerIP, kafkaConsumerPort, consumerGroupId, sessionTimeout, pollTimeLimit, topicName);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
