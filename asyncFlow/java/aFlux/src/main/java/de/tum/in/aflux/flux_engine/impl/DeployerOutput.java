
/*
 * aFlux: JVM based IoT Mashup Tool
 * Copyright 2019 Tanmaya Mahapatra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tum.in.aflux.flux_engine.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import de.tum.in.aflux.flux_engine.model.PrintItem;
import org.springframework.stereotype.Component;

/**
* Class to send messages from deployer to aFlux console
 */

@Component
public class DeployerOutput{

    public ConcurrentLinkedQueue<PrintItem> outputList;
    public DeployerOutput(){
        this.outputList=new ConcurrentLinkedQueue<PrintItem>();
    }

    public void add(String message){
        this.outputList.add(new PrintItem(message));
    }

    public ConcurrentLinkedQueue<PrintItem> getOutputList() {
        return this.outputList;
    }


    public List<String> getMessageList() {
        List<String> result=new ArrayList<String>();
        while (!getOutputList().isEmpty()) {
            result.add(getOutputList().poll().toString());
        }
        return result;
    }
}