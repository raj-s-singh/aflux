/*
 * aFlux: JVM based IoT Mashup Tool
 * Copyright 2019 Tanmaya Mahapatra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package de.tum.in.aflux.flux_engine.impl;


import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;

import org.apache.flink.api.common.JobID;
import org.apache.flink.api.common.JobStatus;
import org.apache.flink.client.deployment.StandaloneClusterId;
import org.apache.flink.client.program.PackagedProgram;
import org.apache.flink.client.program.PackagedProgramUtils;
import org.apache.flink.client.program.rest.RestClusterClient;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.JobManagerOptions;
import org.apache.flink.configuration.RestOptions;
import org.apache.flink.runtime.jobgraph.JobGraph;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.tum.in.aflux.flux_engine.mapper.FluxActorMapper;
import de.tum.in.aflux.flux_engine.mapper.MapperEntry;
import de.tum.in.aflux.model.FlowActivity;
import de.tum.in.aflux.model.FlowElement;
import de.tum.in.aflux.model.FlowElementProperty;
import de.tum.in.aflux.model.FlowJob;
import de.tum.in.aflux.service.FluxJobService;

@Component
public class JobDeployerImpl{

    @Autowired
    public DeployerOutput messages;// = new DeployerOutput();

    @Autowired
    private FluxJobService jobService;


    private String flinkclusterIP;
    private int flinkclusterPort;
    private KafkaConsumer<String,String> consumer;
    private ConsumerRecords<String, String> kafkaRecords;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static final Configuration FLINK_CONFIG = new Configuration();
    private String mainClassName;
    private Map<Integer, String> executionMap = new HashMap<>();
    private FlowJob flowJob;
    private FlowActivity activity;
 
    
    /** 
    * Deploy the Flink Job to flink cluster after configuring the Cluster Client
    * Return the name of the main class in the Flink Job if execution Fails
    * Else it returns Success
    * @param flinkClusterIP
    * @param flinkClusterPort
    * @param jarFile
    * @param parallelism
    * @param jobName
    * @param activityID
    */
    public FlowJob deployFlinkJob(String flinkClusterIP, String flinkClusterPort, String jarFile, String parallelism, String jobName, Long activityID){
        var flinkClusterParallelism=0;

        this.flinkclusterIP=flinkClusterIP;
        this.flinkclusterPort=Integer.parseInt(flinkClusterPort);
        flinkClusterParallelism=Integer.parseInt(parallelism);
        this.flowJob = this.jobService.getByName(jobName);
        this.activity=this.jobService.getActivity(this.flowJob,activityID);


        this.messages.add("Configuring Flink Cluster Client with following paramters :");
        this.messages.add("FlinkClusterIP -> "+flinkClusterIP);
        this.messages.add("FlinkClusterPort -> "+flinkClusterPort);
        this.messages.add("jarFile -> "+jarFile);
        this.messages.add("parallelism -> "+parallelism);
        this.messages.add("jobName->"+jobName);
        this.messages.add("flowActivity->"+this.activity);

        FLINK_CONFIG.setString(JobManagerOptions.ADDRESS, this.flinkclusterIP);
        FLINK_CONFIG.setInteger(RestOptions.PORT, this.flinkclusterPort);
        FLINK_CONFIG.setInteger(RestOptions.RETRY_MAX_ATTEMPTS, 3);

        
        this.messages.add("Configuring the Flink Cluster");
        try(RestClusterClient<StandaloneClusterId> flinkClient = new RestClusterClient<>(FLINK_CONFIG, StandaloneClusterId.getInstance());
            PackagedProgram program = PackagedProgram.newBuilder()
                    .setJarFile(new File(jarFile))
                    .build();) {
            this.messages.add("Creating a Packaged Program");
            this.mainClassName = program.getMainClassName();

            this.messages.add("Creating the Job Graph");
            JobGraph jobGraph = PackagedProgramUtils.createJobGraph(program, FLINK_CONFIG, flinkClusterParallelism, false);

            this.messages.add("Submitting the to Flink Client");
            JobID jobId = flinkClient.submitJob(jobGraph).get();
            this.messages.add("Job submitted with Job ID %s" + jobId.toString());

            JobStatus jobStatus=flinkClient.getJobStatus(jobId).get();
            log.debug("Job Status - {}",jobStatus);

            // long end = System.currentTimeMillis()+30000;
            while(!jobStatus.isTerminalState()) {
                TimeUnit.SECONDS.sleep(5);
                jobStatus=flinkClient.getJobStatus(jobId).get();
                log.debug("Job Status - {}",jobStatus);
            }

            if(jobStatus.toString().equals("FAILED")){
                log.debug("Job Execution Failed due to errors.");
                getStackTrace(jobId.toString());
                return modifyFlowActivity(identifyFaultyActor());
            }

            // this.messages.add("Initializing a Kafka Consumer");
            // initializeKafkaConsumer("60000");
            // this.messages.add("Subscribe to Kafka Topic");
            // subscribeKafkaTopics(Arrays.asList("wiki-result"));
            
            // 

            // this.messages.add("Consuming the logs");
            // consumeKafkaTopics(60000);
            // this.messages.add("Disconnecting Kafka Consumer");
            // disconnectConsumer();
        }

        catch(Exception e){
            this.messages.add(e.toString());
            log.error(e.toString(),e);
        }

        return this.flowJob;

    }
    
    public void consumeKafkaTopic(String kafkaConsumerIP, String kafkaConsumerPort, String consumerGroupId, String sessionTimeout, String pollTimeLimit, String topicName){
            this.messages.add("Initializing a Kafka Consumer");
            this.messages.add("kafkaConsumerIP -> "+ kafkaConsumerIP);
            this.messages.add("kafkaConsumerPort -> " + kafkaConsumerPort);
            this.messages.add("consumerGroupId -> " + consumerGroupId);
            this.messages.add("sessionTimeout -> " + sessionTimeout);
            this.messages.add("pollTimeLimit -> " + pollTimeLimit);
            this.messages.add("topicName -> " + topicName);

            initializeKafkaConsumer(kafkaConsumerIP,kafkaConsumerPort, consumerGroupId, sessionTimeout);
            this.messages.add("Subscribe to Kafka Topic");
            subscribeKafkaTopics(Arrays.asList(topicName));
            
            

            this.messages.add("Consuming the logs");
            startConsumingTopic(Duration.ofMillis(Long.parseLong(pollTimeLimit)));
            this.messages.add("Disconnecting Kafka Consumer");
            disconnectConsumer();

    }
    
    /** 
     * @param reconnectionInterval
     */
    public void initializeKafkaConsumer(String kafkaConsumerIP, String kafkaConsumerPort, String consumerGroupId, String sessionTimeout){
        Properties props = new Properties();
        props.put("bootstrap.servers", kafkaConsumerIP+":"+kafkaConsumerPort );//"localhost:9092"
        props.put("group.id", consumerGroupId);//"test-consumer-group"
        props.put("enable.auto.commit", "false");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", Integer.parseInt(sessionTimeout));
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("auto.offset.reset","earliest");

        this.messages.add("Initializing a Kafka Consumer");
        this.consumer = new KafkaConsumer<>(props);


    }

    
    /** 
     * @param pollTimeLimit
     */
    public void subscribeKafkaTopics(List<String> topicNames){ this.consumer.subscribe(topicNames); }

    
    /** 
     * @param pollTimeLimit
     */
    public void startConsumingTopic(Duration pollTimeLimit) {

        while (this.consumer.assignment().isEmpty()) {
            this.consumer.poll(Duration.ofMillis(0));
        }
        this.consumer.seekToBeginning(this.consumer.assignment());
        this.kafkaRecords = this.consumer.poll(pollTimeLimit);

        for (ConsumerRecord<String, String> consumerRecord : this.kafkaRecords) {

            // print the offset,key and value for the consumer records.
            this.messages.add(String.format("offset = %d, key = %s, value = %s ",
                    consumerRecord.offset(), consumerRecord.key(), consumerRecord.value()));

        }
    }

    public void disconnectConsumer() {
        this.consumer.close();
    }


    
    
    /** 
     * @param fluxOutput
     * @return List<String>
     */
    public List<String> concatMessages(List<String> fluxOutput) {
        List<String> newList = new ArrayList<String>();
        List<String> deployerOutput = this.messages.getMessageList();

        if(!fluxOutput.isEmpty()) {newList.addAll(fluxOutput);}
        if(!deployerOutput.isEmpty()){newList.addAll(deployerOutput);}

        return newList;
    }
    
    /**
     * 
     * @param jobID
     * @throws Exception
     */
    private void getStackTrace(String jobID) throws Exception{
        URL url = new URL("http://"+this.flinkclusterIP+":"+this.flinkclusterPort+"/jobs/"+jobID+"/exceptions");
        HttpURLConnection conn = (HttpURLConnection)url.openConnection(); 
        conn.setRequestMethod("GET"); 
        conn.connect(); 
        int responsecode = conn.getResponseCode();
        if(responsecode != 200) throw new RuntimeException("HttpResponseCode: " +responsecode);
        else{
            Scanner sc = new Scanner(url.openStream());
            String inline="";
            while(sc.hasNext()){
                inline+=sc.nextLine();
            }
            // log.debug("\nJSON data in string format");
            // log.debug(inline);
            sc.close();
            
            JSONParser parse = new JSONParser(); 
            JSONObject jsonObj = (JSONObject)parse.parse(inline);
            JSONArray jsonArrAllExceptions = (JSONArray) jsonObj.get("all-exceptions");
            
            //Parse Exception Object
            for(int i=0;i<jsonArrAllExceptions.size();i++){
                JSONObject jsonException = (JSONObject)jsonArrAllExceptions.get(i);
                String exceptionString = (String) jsonException.get("exception");
                this.messages.add("Job Failed due to - "+exceptionString);
                String[] arrOfExceptionString = exceptionString.split("\n\t", 0);
  
				for (String exception : arrOfExceptionString){
					if (exception.contains(this.mainClassName)){ 
                        // Parse the exception to find the line number of error.
                        this.executionMap.put(Integer.parseInt(
                            exception.substring((exception.indexOf(".java:"))+6,exception.indexOf(")"))),exceptionString);
                    }
                }
			}
        }

    }

    /**
     * 
     * @param actorMap
     * @return Map<String,String>
     */
    public Map<Integer,String> identifyFaultyActor(){
        Map<Integer, String> faultyActorMap= new HashMap<>();
        for (Map.Entry<Integer,String> executionMapElement: this.executionMap.entrySet()){
            for(MapperEntry fluxMapEntry: FluxActorMapper.getFluxMap()){
                if(fluxMapEntry.getActorBeginPosition()<= executionMapElement.getKey() 
                && executionMapElement.getKey() <=fluxMapEntry.getActorEndPosition()){

                    faultyActorMap.put(fluxMapEntry.getActorNo(), executionMapElement.getValue());
                }

            }
        }
        return faultyActorMap;
    }
    
    public FlowJob modifyFlowActivity(Map<Integer,String> faultyActorMap){
        String color = "#E84343";
        String nameSuffix = " (*)";


        List<FlowElement> orderedElements = FluxJobValidator.orderedListOfElements(this.activity.getConnectors());
        FlowElement flaggedFlowElement;

        for(Map.Entry<Integer,String> faultyActor: faultyActorMap.entrySet()){
            flaggedFlowElement=orderedElements.get(faultyActor.getKey()-1);
            for (FlowElementProperty property : flaggedFlowElement.getProperties()) {
                if (property.getName().equals("color"))
                    property.setValue(color);
                else if (property.getName().equals("name"))
                    property.setValue(property.getValue().split(" \\(\\*\\)")[0] + nameSuffix);
            }
            //flaggedFlowElement.setColor(color);
            flaggedFlowElement.setErrors(faultyActor.getValue());
            orderedElements.set(faultyActor.getKey()-1, flaggedFlowElement);
        }
        
        FluxJobValidator.replaceElements(this.activity, orderedElements);
        //this.flowJob.getActivities().set(this.activity.getIndex()-1, this.activity);
        return this.flowJob;
    }
}