import React, { Component } from 'react';
import update from 'immutability-helper';
import '../AsyncFlowsApp.css';

class FlinkDeploymentConfigDialog extends Component{

    constructor(props){
        super(props);
        this.state={
            jarPath : '~',
            jarFile : null,
            flinkClusterIP : 'localhost',
            flinkClusterPort : '8081',
            parallelism : '1',
        };
        this.onChangeIP = this.onChangeIP.bind(this);
        this.onChangePort = this.onChangePort.bind(this);
        this.onChangeParallelism = this.onChangeParallelism.bind(this);

    }

    onChangeIP(event){
        this.setState(update(this.state,{$merge: {
            flinkClusterIP: event.target.value, 
        }}));
    }

    onChangePort(event){
        this.setState(update(this.state,{$merge: {
            flinkClusterPort: event.target.value, 
        }}));
    }

    onChangeParallelism(event){
        this.setState(update(this.state,{$merge: {
            parallelism: event.target.value, 
        }}));
    }

    selectFile(event) {
		var inputFileElement=document.getElementById('uploadJarButton');
        var fullPath = inputFileElement.value;
		var fileToUpload = inputFileElement.files[0];
    	this.setState(update(this.state,{$merge: {
            jarPath: fullPath, 
            jarFile: fileToUpload
        }}));
	}

    componentDidMount() {
		this.inputLabel.focus();
	}

    render(){
        var styleRect={opacity:0,fill:"#4387fd"}
        return (
            <div className="configureFlinkJobDialogStyle" tabIndex="0" >
                <div className="JobSelectorHeader">
                    <div className="CloseIcon" onMouseDown={(event) =>this.props.closeDialog()}>
                        <span className="toolTipClose">Close</span>
                        <div role="button" className="JobSelecta-b-c" tabIndex="0" >
                            <div className="Jm-vu-db-Oh">
                                <svg width="35px" height="35px">
                                    <g ><polygon points="10.5,11.914 13.5,14.914 14.914,13.5 11.914,10.5 14.914,7.5 13.5,6.086 10.5,9.086 7.5,6.086 6.085,7.5 9.085,10.5 6.085,13.5 7.5,14.914 " id="polygon6" transform="translate(-2.9561186,-3.5084746)"/>
                                        <rect width="31" height="31" id="rect8" x="0" y="-6" style={styleRect} />
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div className="flinkConfigDialogMainContent">
                        <div className="DialogContent" >
                            <div>
                                <div>
                                    <label>Flink Cluster IP:</label>
                                    <input type="text" className="flinkClusterIPInput" value={this.state.flinkClusterIP} onChange={this.onChangeIP}/>
                                </div>
                                <div>
                                    <label>Flink Cluster Port:</label>
                                    <input type="text" className="flinkClusterPortInput" value={this.state.flinkClusterPort} onChange={this.onChangePort}/>
                                </div>
                                <div>
                                    <label>Flink Cluster parallelism:</label>
                                    <input type="text" className="flinkClusterParallelism" value={this.state.parallelism} onChange={this.onChangeParallelism}/>
                                </div>

                                <div id="fileUploadDiv" className="AcceptButton" style={{height:"29px",display:"inline-block"}}>
                                    <label ref={(input) => {this.inputLabel=input;}} htmlFor="uploadJarButton" >{this.props.label}</label>
                                </div>
                                <span style={{marginLeft:"10px"}}>{this.state.jarPath}</span>
                                <input id="uploadJarButton" type="file" name="pluginFile" accept=".jar" style={{visibility:"hidden"}} onChange={(event)=>this.selectFile(event)}/>
                                <div>
                                    <input type="button" className="AcceptButton" value="Accept" onMouseDown={(event) =>this.props.accept(
                                        this.state.flinkClusterIP,
                                        this.state.flinkClusterPort,
                                        this.state.jarFile,
                                        this.state.jarPath,
                                        this.state.parallelism)}/>
                                    <input type="button" className="CancelButton" value="Cancel" onMouseDown={(event) =>this.props.closeDialog()}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FlinkDeploymentConfigDialog;