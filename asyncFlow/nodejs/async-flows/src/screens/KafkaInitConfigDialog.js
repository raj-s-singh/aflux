import React, { Component } from 'react';
import update from 'immutability-helper';
import '../AsyncFlowsApp.css';

class KafkaInitConfigDialog extends Component{

    constructor(props){
        super(props);
        this.state={
            consumerGroupId : 'test-consumer-group',
            sessionTimeout : '60000',
            kafkaConsumerIP : 'localhost',
            kafkaConsumerPort : '9092',
            topicName : 'wiki-result',
            pollTimeLimit : '60000',
        };
        this.onChangeIP = this.onChangeIP.bind(this);
        this.onChangePort = this.onChangePort.bind(this);
        this.onChangeGrpId = this.onChangeGrpId.bind(this);
        this.onChangeTimeout = this.onChangeTimeout.bind(this);
        this.onChangePollTime = this.onChangePollTime.bind(this);
        this.onChangeTopicName = this.onChangeTopicName.bind(this);

    }

    componentDidMount(){
        this.inputLabel.focus();
    }

    onChangeIP(event){
        this.setState(update(this.state,{$merge: {
            kafkaConsumerIP: event.target.value, 
        }}));
    }

    onChangePort(event){
        this.setState(update(this.state,{$merge: {
            kafkaConsumerPort: event.target.value, 
        }}));
    }

    onChangeGrpId(event){
        this.setState(update(this.state,{$merge: {
            consumerGroupId: event.target.value, 
        }}));
    }

    onChangeTimeout(event){
        this.setState(update(this.state,{$merge: {
            sessionTimeout: event.target.value, 
        }}));
    }

    onChangePollTime(event){
        this.setState(update(this.state,{$merge: {
            pollTimeLimit: event.target.value, 
        }}));
    }

    onChangeTopicName(event){
        this.setState(update(this.state,{$merge: {
            topicName: event.target.value, 
        }}));
    }

    render(){
        var styleRect={opacity:0,fill:"#4387fd"}
        return (
            <div className="configureFlinkJobDialogStyle" tabIndex="0" >
                <div className="JobSelectorHeader">
                    <div className="CloseIcon" onMouseDown={(event) =>this.props.closeDialog()}>
                        <span className="toolTipClose">Close</span>
                        <div role="button" className="JobSelecta-b-c" tabIndex="0" >
                            <div className="Jm-vu-db-Oh">
                                <svg width="35px" height="35px">
                                    <g ><polygon points="10.5,11.914 13.5,14.914 14.914,13.5 11.914,10.5 14.914,7.5 13.5,6.086 10.5,9.086 7.5,6.086 6.085,7.5 9.085,10.5 6.085,13.5 7.5,14.914 " id="polygon6" transform="translate(-2.9561186,-3.5084746)"/>
                                        <rect width="31" height="31" id="rect8" x="0" y="-6" style={styleRect} />
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div className="flinkConfigDialogMainContent">
                        <div className="DialogContent" >
                            <div>
                                <div>
                                    <label>Kafka Consumer IP:</label>
                                    <input type="text" className="kafkaConsumerIP" value={this.state.kafkaConsumerIP} onChange={this.onChangeIP} ref={(input) => {this.inputLabel=input;}}/>
                                </div>
                                <div>
                                    <label>Kafka Consumer Port:</label>
                                    <input type="text" className="kafkaConsumerPort" value={this.state.kafkaConsumerPort} onChange={this.onChangePort}/>
                                </div>
                                <div>
                                    <label>Consumer Group ID:</label>
                                    <input type="text" className="consumerGroupId" value={this.state.consumerGroupId} onChange={this.onChangeGrpId}/>
                                </div>
                                <div>
                                    <label>Session Timeout(ms):</label>
                                    <input type="text" className="sessionTimeout" value={this.state.sessionTimeout} onChange={this.onChangeTimeout}/>
                                </div>
                                <div>
                                    <label>Poll Time Limit(ms):</label>
                                    <input type="text" className="pollTimeLimit" value={this.state.pollTimeLimit} onChange={this.onChangePollTime}/>
                                </div>
                                <div>
                                    <label>Topic Name:</label>
                                    <input type="text" className="topicName" value={this.state.topicName} onChange={this.onChangeTopicName}/>
                                </div>
                                
                                <div>
                                    <input type="button" className="AcceptButton" value="Accept" onMouseDown={(event) =>this.props.accept(
                                        this.state.kafkaConsumerIP,
                                        this.state.kafkaConsumerPort,
                                        this.state.consumerGroupId,
                                        this.state.sessionTimeout,
                                        this.state.pollTimeLimit,
                                        this.state.topicName)}/>
                                    <input type="button" className="CancelButton" value="Cancel" onMouseDown={(event) =>this.props.closeDialog()}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default KafkaInitConfigDialog;